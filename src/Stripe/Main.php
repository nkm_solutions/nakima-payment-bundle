<?php
declare(strict_types=1);
namespace Nakima\PaymentBundle\Stripe;

/**
 * @author xgonzalez@nakima.es
 */

use Stripe\Account;
use Stripe\Charge;
use Stripe\Customer;
use Stripe\Error\Card;
use Stripe\Error\InvalidRequest;
use Stripe\Stripe;
use Stripe\Token;

class Main
{

    public function __construct()
    {
        $this->configure();
    }

    public function configure()
    {
        Stripe::setApiKey(Symfony::getContainer()->getParameter("stripe.secret"));
    }

    /**************************************************************************
     *                                                                        *
     *   ACCOUNT                                                              *
     *                                                                        *
     **************************************************************************/

    public function createAccount()
    {
        try {
            return Account::create(
                [
                    'managed' => false,
                    'country' => 'ES',
                    'email' => 'info@nakima.es',
                ]
            );
        } catch (InvalidRequest $e) {
            return [
                'error' => $e->getMessage(),
            ];
        }
    }

    public function myDetailsAccount()
    {
        return Account::retrieve();
    }

    public function detailsAccount()
    {
        return Account::retrieve("acct_19JLpnLU03wiTJ2d");
    }

    public function createCard()
    {
        //tok_19JMIHJgQ3nHHxYutKpXL3Sf
        $token = Token::create(
            [
                'card' => [
                    'number' => '4242424242424242',
                    'exp_month' => 11,
                    'exp_year' => 2017,
                    'cvc' => 314,
                ],
            ]
        );

        return $token;

        $tokenId = $token->id;

        $customer = Customer::retrieve($this->myDetailsAccount()->id);//$this->myDetailsAccount()->id;
        $card = $customer->sources->create(
            [
                'source' => $tokenId,
            ]
        );

        // tok_19JMIHJgQ3nHHxYutKpXL3Sf

        return $customer;
    }

    /**************************************************************************
     *                                                                        *
     *   CUSTOMER                                                             *
     *                                                                        *
     **************************************************************************/

    public function loadCustomer($email, $token, $tokenType)
    {

        $customer = Customer::create(
            [
                'source' => $token,
                'email' => $email,
            ]
        );

        return $customer;

        $customer = Customer::retrieve($token);

        return $customer;
    }


    /**************************************************************************
     *                                                                        *
     *   CHARGE                                                               *
     *                                                                        *
     **************************************************************************/

    public function chargeCustomer($customer, $amount)
    {
        try {
            return Charge::create(
                [
                    'customer' => $customer,
                    'amount' => $amount,
                    'currency' => 'eur',
                ]
            );
        } catch (Card $e) {
            return [
                'error' => $e->getMessage(),
            ];
        }
    }

    public function loadCharge($charge)
    {
        return Charge::retrieve($charge);
    }
}
