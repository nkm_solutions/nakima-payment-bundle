<?php
declare(strict_types=1);
namespace Nakima\ShopBundle\DataFixtures\ORM;

/**
 * @author Adrià Llaudet Planas <allaudet@nakima.es>
 */

use Nakima\CoreBundle\DataFixtures\ORM\Fixture;
use PaymentBundle\Entity\PaymentStatus;

class LoadPaymentStatusData extends Fixture
{

    public function loadProd(): void
    {
        return;
        $brand = new Brand();
        $brand->setName("test brand 1");
        $brand->setDescription("descripción de test brand 1");
        $brand->setEnabled(false);
        $manager->persist($brand);
        $manager->flush();
    }
}
