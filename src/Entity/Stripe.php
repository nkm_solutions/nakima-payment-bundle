<?php
declare(strict_types=1);
namespace Nakima\PaymentBundle\Entity;

use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\PostFlush;
use Nakima\CoreBundle\Entity\BaseEntity;
use Nakima\CoreBundle\Utils\Symfony;
use Nakima\Utils\Time\DateTime;
use Stripe\Charge;
use Stripe\Customer;
use Stripe\Error\Card;
use Stripe\Stripe as Stripe2;

class Stripe extends BaseEntity
{

    /**
     * @Column(type="string", length=64)
     */
    protected $customer;

    /**
     * @Column(type="nakima_datetime")
     */
    protected $createdAt;

    /**
     * @Column(type="string", length=255)
     */
    protected $email;

    /**
     * @Column(type="string", length=64, nullable=false)
     */
    protected $orderReference;

    /**
     * @Column(type="float", nullable=false)
     */
    protected $productsPrice;

    /**
     * @ManyToOne(
     *     targetEntity="ShopBundle\Entity\ShippingMethod"
     * )
     * @JoinColumn(
     *     name="shippingmethod_id",
     *     referencedColumnName="id",
     *     nullable=false
     * )
     */
    protected $shippingMethod;

    /**
     * @Column(type="float", nullable=false)
     */
    protected $shippingPrice;

    /**
     * @Column(type="float", nullable=false)
     */
    protected $totalPrice;

    /**
     * @Column(type="float", nullable=false)
     */
    protected $amountPaid;

    /**
     * @OneToMany(
     *     targetEntity="ShopBundle\Entity\Order",
     *     mappedBy="stripe"
     * )
     */
    protected $orders;

    /**************************************************************************
     *                                                                        *
     *   CHARGE                                                               *
     *                                                                        *
     **************************************************************************/

    /**
     * @Column(type="string", length=64, nullable=true)
     */
    protected $risk;

    /**
     * @Column(type="string", length=3, nullable=true)
     */
    protected $country;

    /**
     * @Column(type="string", length=4, nullable=true)
     */
    protected $lastDigits;

    /**
     * @Column(type="string", length=64, nullable=true)
     */
    protected $status;

    /**
     * @Column(type="boolean", nullable=true)
     */
    protected $refunded;

    /**
     * @Column(type="nakima_datetime", nullable=true)
     */
    protected $purchasedAt;

    /**
     * @Column(type="text", nullable=true)
     */
    protected $observation;


    /**************************************************************************
     *                                                                        *
     *   Custom Functions                                                     *
     *                                                                        *
     **************************************************************************/

    public function __construct()
    {
        Stripe2::setApiKey(Symfony::getContainer()->getParameter("stripe.secret"));
        $this->createdAt = new DateTime;
        $this->setObservation("");
        $this->setAmountPaid(0.0);
    }

    public static function create($token, $email)
    {
        $stripe = new \PaymentBundle\Entity\Stripe;
        $stripe->_create($token, $email);

        return $stripe;
    }

    public static function pay($token, $email, $amount)
    {
        $stripe = self::create($token, $email);
        $stripe->_pay($amount);

        return $stripe;
    }

    public function _create($token, $email)
    {

        $customer = Customer::create(
            [
                'source' => $token,
                'email' => $email,
            ]
        );

        $this->setCustomer($customer->id);
        $this->setEmail($email);

        return $this;
    }

    public function _pay($amount)
    {
        $amount = round($amount * 100);
        $this->setPurchasedAt(new DateTime());
        try {
            $charge = Charge::create(
                [
                    'customer' => $this->customer,
                    'amount' => $amount,
                    'currency' => 'eur',
                ]
            );

            $this->setRefunded(false);
            $this->setStatus($charge->status);
            $this->setLastDigits($charge->source->last4);
            $this->setCountry($charge->source->country);
            $this->setRisk($charge->outcome->risk_level);
        } catch (Card $e) {
            $this->setObservation($e->getMessage());
        }

        return $this;
    }

    /**************************************************************************
     *                                                                        *
     *   Getters & Setters                                                    *
     *                                                                        *
     **************************************************************************/

    public function getCustomer()
    {
        return $this->customer;
    }

    public function setCustomer($customer)
    {
        $this->customer = $customer;

        return $this;
    }

    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    public function getRisk()
    {
        return $this->risk;
    }

    public function setRisk($risk)
    {
        $this->risk = $risk;

        return $this;
    }

    public function getCountry()
    {
        return $this->country;
    }

    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    public function getLastDigits()
    {
        return $this->lastDigits;
    }

    public function setLastDigits($lastDigits)
    {
        $this->lastDigits = $lastDigits;

        return $this;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    public function getRefunded()
    {
        return $this->refunded;
    }

    public function setRefunded($refunded)
    {
        $this->refunded = $refunded;

        return $this;
    }

    public function getPurchasedAt()
    {
        return $this->purchasedAt;
    }

    public function setPurchasedAt($purchasedAt)
    {
        $this->purchasedAt = $purchasedAt;

        return $this;
    }

    public function getObservation()
    {
        return $this->observation;
    }

    public function setObservation($observation)
    {
        $this->observation = $observation;

        return $this;
    }

    public function getBillAddress()
    {
        return $this->billAddress;
    }

    public function setBillAddress($billAddress)
    {
        $this->billAddress = $billAddress;

        return $this;
    }

    public function getShippingAddress()
    {
        return $this->shippingAddress;
    }

    public function setShippingAddress($shippingAddress)
    {
        $this->shippingAddress = $shippingAddress;

        return $this;
    }

    public function getOrderReference()
    {
        return $this->orderReference;
    }

    public function setOrderReference($orderReference)
    {
        $this->orderReference = $orderReference;

        return $this;
    }

    public function getProductsPrice()
    {
        return $this->productsPrice;
    }

    public function setProductsPrice($productsPrice)
    {
        $this->productsPrice = $productsPrice;

        return $this;
    }

    public function getShippingMethod()
    {
        return $this->shippingMethod;
    }

    public function setShippingMethod($shippingMethod)
    {
        $this->shippingMethod = $shippingMethod;

        return $this;
    }

    public function getShippingPrice()
    {
        return $this->shippingPrice;
    }

    public function setShippingPrice($shippingPrice)
    {
        $this->shippingPrice = $shippingPrice;

        return $this;
    }

    public function getTotalPrice()
    {
        return $this->totalPrice;
    }

    public function setTotalPrice($totalPrice)
    {
        $this->totalPrice = $totalPrice;

        return $this;
    }

    public function getAmountPaid()
    {
        return $this->amountPaid;
    }

    public function setAmountPaid($amountPaid)
    {
        $this->amountPaid = $amountPaid;

        return $this;
    }

    public function incAmountPaid($amountPaid)
    {
        $this->amountPaid += $amountPaid;

        return $this;
    }

    public function addOrder(\ShopBundle\Entity\Order $order)
    {
        $this->orders[] = $order;

        return $this;
    }

    public function removeOrder(\ShopBundle\Entity\Order $order)
    {
        $this->orders->removeElement($order);
    }

    public function getOrders()
    {
        return $this->orders;
    }

}
