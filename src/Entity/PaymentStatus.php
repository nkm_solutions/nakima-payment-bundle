<?php
declare(strict_types=1);
namespace Nakima\PaymentBundle\Entity;

use Nakima\CoreBundle\Entity\BaseEntity;

class PaymentStatus extends BaseEntity
{

    /**
     * @Column(type="string", length=64)
     */
    protected $name;

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }
}
