<?php
declare(strict_types=1);
namespace Nakima\PaymentBundle;

/**
 * @author xgonzalez@nakima.es
 */

use Symfony\Component\HttpKernel\Bundle\Bundle;

class NakimaPaymentBundle extends Bundle {}
